import React from "react";
import { Group } from "react-konva";
import Wall from "./Wall";

function WallGroup() {
  return (
    <Group>
      <Wall x="20" y="20" length={120} isHorizontal />
      <Wall x="140" y="16" length={80} rotation={0} />
      <Wall x="20" y="20" length={240} />
      <Wall x="20" y={260} length={50} rotation={-45} />
      <Wall x="55" y={292} length={115} />
      <Wall x="20" y="100" length={40} isHorizontal />
      <Wall x="104" y="100" length={80} isHorizontal />
      <Wall x="56" y="100" length={80} />
      <Wall x="20" y="180" length={80} isHorizontal />
      <Wall x="49" y="405" length={115} />
      <Wall x="49" y="407" length={7} isHorizontal />
      <Wall x="60" y="145" length={55} isHorizontal />
      <Wall x="115" y="125" length={80} />
      <Wall x="115" y="160" length={90} isHorizontal />
      <Wall x="201" y="160" length={15} />
      <Wall x="115" y="205" length={90} isHorizontal />
      <Wall x="115" y="182.5" length={35} isHorizontal />
      <Wall x="202" y="175" length={340} isHorizontal />
      <Wall x="538" y="175" length={140} />
      <Wall x="555" y="215" length={340} />
      <Wall x="49" y="524" length={70} isHorizontal />
      <Wall x="49" y="454" length={190} isHorizontal />
      <Wall x="115" y="454" length={70} />
      <Wall x="235" y="454" length={22} />
      <Wall x="237" y="476" length={40} isHorizontal />
      <Wall x="277" y="450" length={50} />
      <Wall x="277" y="500" length={70} isHorizontal />
      <Wall x="347" y="476" length={100} isHorizontal />
      <Wall x="347" y="476" length={24} />
      <Wall x="443" y="450" length={24} />
      <Wall x="443" y="450" length={35} isHorizontal />
      <Wall x="478" y="446" length={110} />
      <Wall x="479" y="556" length={80} isHorizontal />
    </Group>
  );
}

export default WallGroup;
