import React from "react";
import Desk from "./Desk";
import { Group } from "react-konva";

function SixDesks({ chairs, x, y, isHorizontal, selectedDesk, setSelectedDesk }) {
  return (
    <Group x={x} y={y} rotation={isHorizontal ? 90: 0}>
      <Desk x={0} y={0} id={chairs[0]} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      <Desk x={0} y={26} id={chairs[1]} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      <Desk x={0} y={52} id={chairs[2]} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      <Desk x={60} y={25} id={chairs[3]} isRotate selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      <Desk x={60} y={51} id={chairs[4]} isRotate selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      <Desk x={60} y={77} id={chairs[5]} isRotate selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
    </Group>
  );
}

export default SixDesks;
