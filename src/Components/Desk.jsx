import React, { useEffect, useState } from "react";
import { Rect, Circle, Group } from "react-konva";

function Desk({ x, y, id, isRotate, selectedDesk, setSelectedDesk }) {
  const [color, setColor] = useState("#f5f5f5");
  const handleClick = () => {
    setSelectedDesk(id);
  };

  useEffect(() => {
    if (selectedDesk === id) {
      setColor("#77dd77");
    }else{
      setColor("#f5f5f5");
    }
  }, [selectedDesk]);
  return (
    <Group x={x} y={y} rotation={isRotate ? 180 : 0}>
      <Rect x={0} y={6} width={4} height={12} fill="#010101" />
      <Rect x={1} y={9} width={4} height={7} fill="#010101" rotation={260} />
      <Rect x={0} y={18} width={4} height={7} fill="#010101" rotation={-70} />
      <Rect x={7} y={0} width={22} height={25} fill="#6d7ca4" />
      <Circle
        x={18}
        y={12}
        height={18}
        width={18}
        fill={color}
        onClick={handleClick}
        onTap={handleClick}
      />
    </Group>
  );
}

export default Desk;
