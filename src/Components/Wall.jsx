import React from 'react';
import { Rect } from 'react-konva';

function Wall( {x, y, length, rotation, isHorizontal} ) {
    const rotationAngle = isHorizontal ? 270 : rotation;
    return ( 
        <Rect
          x={x}
          y={y}
          width={4}
          rotation={rotationAngle}
          height={length}
          fill="#010101"
        />
     );
}

export default Wall;