import React from "react";
import { Stage, Layer } from "react-konva";
import WallGroup from "./Components/WallGroup";
import SixDesks from "./Components/SixDesks";

const App = () => {
  const [selectedDesk, setSelectedDesk] = React.useState(null);
  return (
    <Stage width={window.innerWidth} height={window.innerHeight}>
      <Layer>
        <WallGroup />
        <SixDesks chairs={[0, 1, 2, 3, 4, 5]} x={120} y={235} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[6, 7, 8, 9, 10, 11]} x={120} y={350} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[12, 13, 14, 15, 16, 17]} x={250} y={235} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[18, 19, 20, 21, 22, 23]} x={250} y={350} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[24, 25, 26, 27, 28, 29]} x={120} y={350} selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[30, 31, 32, 33, 34, 35]} x={450} y={235} isHorizontal selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
        <SixDesks chairs={[36, 37, 38, 39, 40, 41]} x={450} y={350} isHorizontal selectedDesk={selectedDesk} setSelectedDesk={setSelectedDesk}/>
      </Layer>
    </Stage>
  );
};

export default App;
